package main

import (
	"adai.design/homemaster/client"
	"adai.design/homemaster/coordinator"
	"adai.design/homemaster/log"
	"adai.design/homemaster/modules"
	"adai.design/homemaster/server"
)

func main() {
	err := modules.Start()
	coordinator.StartCoordinator()
	if err != nil {
		log.Fatal(err)
	}
	defer modules.Stop()
	defer coordinator.StopCoordinator()

	client.Start()
	server.Start()

	//for {
	//	select {
	//	case <-time.After(time.Second * 2):
	//		var stat runtime.MemStats
	//		runtime.ReadMemStats(&stat)
	//		log.Printf("alloc(%0.2fM) total(%0.2fM) sys(%0.2fM) lookups(%d) maollocs(%0.2fM) frees(%0.2fM)\n",
	//			float32(stat.Alloc)/1024/1024,
	//			float32(stat.TotalAlloc)/1024/1024,
	//			float32(stat.Sys)/1024/1024,
	//			stat.Lookups,
	//			float32(stat.Mallocs)/1024/1024,
	//			float32(stat.Frees)/1024/1024)
	//	}
	//}

	select {}

	log.Info("the end")
}
