# HomeMaster V2.0

该项目属于智能家居定制项目的一部分，该项目还包括:

- [**homemaster 设备接入网关, 采用Go+C开发**](https://gitee.com/adaidesigner/homemaster)
- [**homemaster-driver 网关硬件设备驱动, 由C编写的linux内核模块**](https://gitee.com/adaidesigner/homemaster-driver)
- [**zigbee3.0-coordinator 网关协调器部分, 芯片采用NXP的JN5169**](https://gitee.com/adaidesigner/zigbee3.0-coordinator)
- [**jarvis 家庭控制中枢服务端部分, 采用Go编写**](https://gitee.com/adaidesigner/jarvis)
`取名来源: JARVIS(贾维斯)钢铁侠托尼的AI助理，幻视的核心软件`
- [**home 家居ios端应用程序,兼容iphone和ipad, 由swift编写 (半成品,未完工)**](https://gitee.com/adaidesigner/home)

## 智能家居定制项目介绍网址: [**https://adai.design/design**](http://adai.design/design)
![image](doc/web-adai-design-explore.jpg)

## 智能家居定制-网关V2019版
- 网关方案: MT7688+Openwrt(linux)
- 开发语言: Go编写应用层+C编写硬件驱动
- 网关外观: 由Creo设计，采用3D打样+白色磨砂喷漆

![image](doc/product.jpg)

- 三维尺寸: 长(86mm) 宽(82mm) 高(86mm)  重量(365克)  材料(未来8000树脂+白色喷漆)
- 处理器: MT7688 RAM(128MB) + Flash(32MB)  Openwrt 15.10 (linux 3.18.29)
- Zigbee协调器: JN5169 RAM(32kB) + Flash(512kB) FreeRTOS
- [**PCB原理图导出文件: homemaster.pdf**](doc/homemaster.pdf)

![image](doc/hardware.jpg)

## 项目框架
- 项目主要采用Go语言开发，可在电脑上开发调试，再交叉编译到linux上运行
- 框架参考了Homekit的设备模型 Accessory-Service-Characteristic 三层结构

![image](doc/architecture.jpg)

## 编译与运行

- 编译工程: make
- 编译驱动: make ko
- 部署: make remote (`scp -r build/files/tmp/homemaster root@home.local:/tmp/`)

### 局域网获取与控制设备状态
- 参考: server/message_test.go
- 设备控制操作示例(控制设备): aid(配件id) sid(服务id) cid(属性id) value(目标值:1开0关)
 ```
 {
  "path": "characteristics",
  "method": "post",
  "data": [
    {
      "aid": "00158d000288fb3d",
      "sid": 1,
      "cid": 1,
      "value": 0
    }
  ]
}
 ```

### 历史记录
- 2019-10-12: 支持宜家飞利浦灯的色温控制
- 2018-08-29: Zigbee3.0设备接入, 移除433模块
- 2018-03-04: 创建项目，接入433设备

## 智能家居定制项目介绍网址: [**https://adai.design/design**](http://adai.design/design)


