package coordinator

import (
	"adai.design/homemaster/container/accessory"
	"encoding/binary"
	"adai.design/homemaster/log"
	"adai.design/homemaster/modules"
)

// 小米温湿度传感器

type zigbeeHTSensor struct {
	*ZigbeeDescriptor
	acc *accessory.HTSensor

	temperature float32
	humidify float32

}

func (z *zigbeeHTSensor) update(data []byte) error {
	srcAddr := binary.BigEndian.Uint16(data[1:])
	endpoint := data[3]

	clusterId := binary.BigEndian.Uint16(data[4:6])
	value := binary.BigEndian.Uint16(data[11:13])

	log.Printf("attribute: netaddr(%04x) endpoint(%d) data(%x) clusterId(%04x) value(%0.2f)\n", srcAddr, endpoint, data, clusterId, float32(value)/100)
	if clusterId == 0x0402 {
		z.temperature = float32(value) / 100
		z.acc.HTSensor.Temperature.UpdateValue(z.temperature)
	} else if clusterId == 0x0405 {
		z.humidify = float32(value) / 100
		z.acc.HTSensor.Humidity.UpdateValue(z.humidify)
	}

	modules.SetClockHT(z.temperature, z.humidify)
	return nil
}

func newHTSensor(zigbee *ZigbeeDescriptor) *zigbeeHTSensor {
	sensor := &zigbeeHTSensor{
		ZigbeeDescriptor: zigbee,
		acc: accessory.NewHTSensor(zigbee.MacAddr),
	}
	return sensor
}


const xiaoMiHTSensorEndpointDefault =
	`    [
			{
                "endpoint": 1,
                "profile_id": "0104",
                "device_id": "5f01",
                "input_cluster": [
                    "0000",
                    "0003",
                    "0019",
                    "ffff",
                    "0012"
                ],
                "output_cluster": [
                    "0000",
                    "0003",
                    "0019",
                    "ffff",
                    "0012",
                    "0300",
                    "0000"
                ],
                "manufacturer": "LUMI",
                "mode": "lumi.sensor_ht",
                "date": "20160516"
            },
            {
                "endpoint": 2,
                "profile_id": "0104",
                "device_id": "5f02",
                "input_cluster": [
                    "0003",
                    "0012"
                ],
                "output_cluster": [
                    "0003",
                    "0012",
                    "0300",
                    "0000"
                ]
            },
            {
                "endpoint": 3,
                "profile_id": "0104",
                "device_id": "5f03",
                "input_cluster": [
                    "0003",
                    "000c",
                    "0003",
                    "000c"
                ],
                "output_cluster": [
                    "0003",
                    "000c",
                    "0300",
                    "0000",
                    "0003",
                    "000c",
                    "0300",
                    "0000"
                ]
            }
        ]`





