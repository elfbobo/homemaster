package accessory

import "adai.design/homemaster/container/service"

const (
	SwitchChan1SevId = 1
	SwitchChan2SevId = 2
	SwitchChan3SevId = 3
)

// 两路开关
type SwitchChan2 struct {
	*Accessory

	Chan01 *service.Switch
	Chan02 *service.Switch
}

func NewSwitchChan2(uuid string) *SwitchChan2 {
	acc := &SwitchChan2{}
	acc.Accessory = New(TypeSwitch, uuid)

	acc.Chan01 = service.NewSwitch()
	acc.AddService(acc.Chan01.Service, SwitchChan1SevId)

	acc.Chan02 = service.NewSwitch()
	acc.AddService(acc.Chan02.Service, SwitchChan2SevId)

	return acc
}

// 三路开关
type SwitchChan3 struct {
	*Accessory
	Chan01 *service.Switch
	Chan02 *service.Switch
	Chan03 *service.Switch
}

func NewSwitchChan3(uuid string) *SwitchChan3 {
	acc := &SwitchChan3{}
	acc.Accessory = New(TypeSwitch, uuid)

	acc.Chan01 = service.NewSwitch()
	acc.AddService(acc.Chan01.Service, SwitchChan1SevId)

	acc.Chan02 = service.NewSwitch()
	acc.AddService(acc.Chan02.Service, SwitchChan2SevId)

	acc.Chan03 = service.NewSwitch()
	acc.AddService(acc.Chan03.Service, SwitchChan3SevId)
	return acc
}

const (
	OutletChan1SevId = 1
)

// 插座
type Outlet struct {
	*Accessory

	Outlet *service.Outlet
}

func NewOutlet(uuid string) *Outlet {
	acc := &Outlet{}

	acc.Accessory = New(TypeOutlet, uuid)

	acc.Outlet = service.NewOutlet()
	acc.AddService(acc.Outlet.Service, OutletChan1SevId)

	return acc
}

// 门磁
const ContactSensorStateSevId = 1

type ContactSensor struct {
	*Accessory
	Contact *service.ContactSensor
}

func NewContactSensor(uuid string) *ContactSensor {
	acc := &ContactSensor{}
	acc.Accessory = New(TypeContactSensor, uuid)

	acc.Contact = service.NewContactSensor()
	acc.AddService(acc.Contact.Service, ContactSensorStateSevId)

	return acc
}

// 门磁
const HTSensorSevId = 1

// HTSensor
type HTSensor struct {
	*Accessory
	HTSensor *service.HTSensor
}

func NewHTSensor(uuid string) *HTSensor {
	acc := &HTSensor{}
	acc.Accessory = New(TypeHTSensor, uuid)

	acc.HTSensor = service.NewHTSensor()
	acc.AddService(acc.HTSensor.Service, HTSensorSevId)

	return acc
}

// 彩色灯
const ColorLightSevId = 1

// LightColor
type LightColor struct {
	*Accessory
	Light *service.LightColor
}

func NewLightColor(uuid string) *LightColor {
	acc := &LightColor{}
	acc.Accessory = New(TypeColorLight, uuid)

	acc.Light = service.NewColorLight()
	acc.AddService(acc.Light.Service, ColorLightSevId)
	return acc
}

// 色温灯
type LightColorTemperature struct {
	*Accessory
	Light *service.LightTemperatureColor
}

func NewLightColorTemperature(uuid string) *LightColorTemperature {
	acc := &LightColorTemperature{}
	acc.Accessory = New(TypeTemperatureColorLight, uuid)

	acc.Light = service.NewLightTemperatureColor()
	acc.AddService(acc.Light.Service, ColorLightSevId)
	return acc
}

const MotionSevId = 1

type MotionSensor struct {
	*Accessory
	Motion *service.Motion
}

func NewMotionSensor(uuid string) *MotionSensor {
	acc := &MotionSensor{}
	acc.Accessory = New(TypeMotionSensor, uuid)

	acc.Motion = service.NewMotion()
	acc.AddService(acc.Motion.Service, MotionSevId)
	return acc
}

// HomeMasterV2

type HomeMasterV2 struct {
	*Accessory
}

func NewHomeMasterV2(uuid string) *HomeMasterV2 {
	acc := &HomeMasterV2{}
	acc.Accessory = New(TypeHomeMasterV2, uuid)
	return acc
}
